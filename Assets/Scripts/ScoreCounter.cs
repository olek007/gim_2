﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreCounter : MonoBehaviour {

    public int totalScore;
    public GameObject text;
    // Use this for initialization
    void Start ()
    {
        totalScore = 0;
    }

    void Update()
    {
        text.GetComponent<Text>().text = "Score: " + totalScore;
        if(Time.timeScale <0.5f)
        {
            text.GetComponent<Text>().text = "Game Over. Total Score: " + totalScore;
        }
    }

    public void AddScore(int score)
    {
        totalScore += score;
    }
}
