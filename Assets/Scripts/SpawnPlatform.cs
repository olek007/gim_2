﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SpawnPlatform : MonoBehaviour
{

    // How long our level is.
    public int maxPlatforms = 20;
    public float time;
    // The actual game object
    public GameObject platform;
    // Range of positions where we will spawn a new platform
    public float horizontalMin = 7.5f;
    public float horizontalMax = 14f;
    public float verticalMin = -6f;
    public float verticalMax = 6f;
    // Where our initial offset is.
    private Vector2 originPosition;

    public GameObject text;

    // Use this for initialization
    void Start()
    {
        originPosition = transform.position;
        time = 0;
        for (int i = 0; i < maxPlatforms; i++)
        {
            Spawn();
        }
    }

    void Update()
    {
        time += Time.deltaTime;
        text.GetComponent<Text>().text = "Remaining time: " + Mathf.Round(60 - time);
        if (time >= 60)
        {
            Time.timeScale = 0.000000001f;
        }
    }

    // Function that generates a new gameobject.
    public void Spawn()
    {
        Vector2 randomPosition = originPosition + new Vector2(Random.Range(horizontalMin, horizontalMax), Random.Range(verticalMin, verticalMax));
        Instantiate(platform, randomPosition, Quaternion.identity);
        platform.GetComponent<SpawnCoins>().Spawn();
        originPosition = randomPosition;
    }


}
