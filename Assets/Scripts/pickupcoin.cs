﻿using UnityEngine;
using System.Collections;

public class pickupcoin : MonoBehaviour {

    // For now just destroy the coin if the player runs into
    // it. Could play a sound, add to a score or more here later!
    public int score;
    public GameObject player;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

	void OnTriggerEnter2D(Collider2D other)
    {
		if (other.gameObject.CompareTag ("Player"))
        {
			Destroy(this.gameObject);
            player.GetComponent<ScoreCounter>().AddScore(score);
		}
	}
}
